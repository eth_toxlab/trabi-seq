#importing necessary modules
import os
import numpy as np
import pandas as pd
import scipy
import sys


import argparse
###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "The path to the folder with input data", required = True)
parser.add_argument("-s", "--sample", help = "The sample (file) name", required = True)
argument = parser.parse_args()

PATH = argument.path
sample = argument.sample


chromosomes = ['chr' + str(i) for i in np.arange(1, 23, 1)] + ["chrX"]


p = "/nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/GLOEseq_all_ET743_AF_data_processed/gene_annotation/"

#PATH = "/nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/GLOEseq_all_ET743_AF_data_processed/Sample_folders/"
prefix = "UMIdedup_UMIremoved_trimmed_"
suffix = "GRCh38.p13_ALL_"

strands = {"sense" : [("plus", "plus"), ("minus", "minus")],
           "antisense" : [("plus", "minus"), ("minus", "plus")]}
#(gene's strand, damage signal's strand)

DATA = pd.DataFrame({})

for s in strands:
            for ss in strands[s]:
                df1 = pd.read_csv(p + "knownGenes_canonTr_" + ss[0] + "_strand.bed", 
                                  sep = "\t", header = None, 
                                  names = ["Chr", "Start", "End", "Gene", "Transcript", "Strand"])
                
                #
                df_up = pd.read_csv(p + "knownGenes_canonTr_2000bp_upstream_TSS_feature.bed", 
                                  sep = "\t", header = None, 
                                  names = ["Chr", "Start_up", "End_up", "Gene", "Transcript", "Strand"])
                df_up = df_up.loc[:, ["Start_up", "End_up", "Gene"]]
                #
                df_down = pd.read_csv(p + "knownGenes_canonTr_2000bp_downstream_TES_feature.bed", 
                                  sep = "\t", header = None, 
                                  names = ["Chr", "Start_down", "End_down", "Gene", "Transcript", "Strand"])
                df_down = df_down.loc[:, ["Start_down", "End_down", "Gene"]]
                #
                df_TSSdown = pd.read_csv(p + "knownGenes_canonTr_2000bp_downstream_TSS_feature.bed", 
                                  sep = "\t", header = None, 
                                  names = ["Chr", "Start_TSSdown", "End_TSSdown", "Gene", "Transcript", "Strand"])
                df_TSSdown = df_TSSdown.loc[:, ["Start_TSSdown", "End_TSSdown", "Gene"]]
                #
                df1 = pd.merge(df1, df_up, on = "Gene", how = "left")
                df1 = pd.merge(df1, df_down, on = "Gene", how = "left")
                df1 = pd.merge(df1, df_TSSdown, on = "Gene", how = "left")
                if df1.shape[0] - df1.dropna().shape[0] > 0:
                    print("Warning 1")
               
            
                df2 = os.path.join(PATH, sample, "bed_and_bedgraph", prefix + sample + suffix + ss[1] + "_strand.bedgraph")
                df2 = pd.read_csv(df2, sep = "\t", header = None, 
                                  names = ["Chr", "Start", "End", "Value", "MAPQ"])
                
                df2 = df2.loc[:, ["Chr", "Start", "End", "Value"]].copy()

                for chrom in chromosomes:
                    df1_ch = df1[df1["Chr"] == chrom].copy().reset_index(drop = True)
                    df2_ch = df2[df2["Chr"] == chrom].copy().reset_index(drop = True)
                    
                    features = ["Gene", "Upstream", "Downstream", "DownstreamTSS"]
                    feature_bounds = [("Start", "End"), ("Start_up", "End_up"), ("Start_down", "End_down"), ("Start_TSSdown", "End_TSSdown")]
                    
                    for index, feature in enumerate(features):
                        a = np.array(df2_ch["Start"].values)
                        
                        start = feature_bounds[index][0]
                        end = feature_bounds[index][1]
                        bl = np.array(df1_ch[start].values)
                        bh = np.array(df1_ch[end].values)
                        i, j = np.where((a[:, None] >= bl) & (a[:, None] < bh))

                        tmp = pd.concat([
                            df2_ch.loc[i, :].reset_index(drop=True),
                            df1_ch.loc[j, :].reset_index(drop=True)
                        ], axis=1)

                        tmp = tmp.loc[:, ["Value", "Gene"]].groupby(by = "Gene").sum().reset_index()
                        tmp = pd.merge(df1_ch, tmp, on = "Gene", how = "left").fillna(0)
                        tmp.loc[:, "Damage"] = tmp["Value"]
                        tmp = tmp.loc[:, ["Gene", "Damage"]]
                        
                        tmp.loc[:, "Feature"] = feature
                        tmp.loc[:, "Sample"] = sample
                        tmp.loc[:, "Strand"] = s
                        
                        DATA = pd.concat([DATA, tmp])
          
            print(sample, s)
            
DATA.to_csv("/nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/GLOEseq_all_ET743_AF_data_processed/gene_related_damage/GLOEseq__damage_summed_per_feature_" + sample + ".csv")