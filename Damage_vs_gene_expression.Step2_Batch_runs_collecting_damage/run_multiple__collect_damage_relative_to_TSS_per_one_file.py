'''
Dr. Vakil Takhaveev
22.02.2023
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "The path to the folder with input data", required = True)
argument = parser.parse_args()

PATH = argument.path

bash_script_templ = '''#!/bin/bash

#SBATCH -n 1
#SBATCH --cpus-per-task=1
#SBATCH --time=00:30:00
#SBATCH --job-name=_SAMPLE_
#SBATCH --mem-per-cpu=5G
#SBATCH --output="/cluster/home/vtakhaveev/DamageSeqAnalysis/GLOEseq/GLOEseq_MayJulyOct2022_Jan2023/Damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_TSS_per_one_file_LOGs/_SAMPLE_.out"
#SBATCH --error="/cluster/home/vtakhaveev/DamageSeqAnalysis/GLOEseq/GLOEseq_MayJulyOct2022_Jan2023/Damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_TSS_per_one_file_LOGs/_SAMPLE_.err"
#SBATCH --open-mode=truncate

module load python/3.7.4

ls /nfs/nas12.ethz.ch/fs1201/green_groups_let_public/Euler/Vakil/GLOEseq_all_ET743_AF_data_processed

python3.7 /cluster/home/vtakhaveev/DamageSeqAnalysis/GLOEseq/GLOEseq_MayJulyOct2022_Jan2023/Damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_TSS_per_one_file.py -p _PATH_ -s _SAMPLE_
'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

for SAMPLE in os.listdir(PATH):
  if "_R1" in SAMPLE and SAMPLE != "20220525.B-o28326_1_13-13-Emma-Vakil_R1.fastq.gz":
    print(SAMPLE)

    sh_name = os.path.join("/cluster/home/vtakhaveev/DamageSeqAnalysis/GLOEseq/GLOEseq_MayJulyOct2022_Jan2023/Damage_vs_gene_expression_MS/Step2_Batch_runs_collecting_damage/collect_damage_relative_to_TSS_per_one_file_LOGs/", SAMPLE + ".sh")
    with open(sh_name, 'w') as job_script:
        job_script.write(
            replace_all(bash_script_templ, {"_SAMPLE_" : SAMPLE, "_PATH_" : PATH})
        )
    
    os.system("sbatch < " + sh_name)
    time.sleep(2)
