'''
Dr. Vakil Takhaveev
14.06.2022

This script appends the UMI stored as the R2 sequence to the R1 read name.

GLOE-seq data analysis.
'''
import argparse
import gzip
from Bio import SeqIO
import os
import pandas as pd

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-i", "--r1", help = "Input R1.fastq file. Unzipped!", required = True)
parser.add_argument("-j", "--r2", help = "Input R2.fastq file. Unzipped!", required = True)
argument = parser.parse_args()
R1file = argument.r1
R2file = argument.r2

R1_parsed = SeqIO.index(R1file, "fastq")
R1_df = pd.DataFrame(R1_parsed)
N_in = R1_df.shape[0]

R2_parsed = SeqIO.index(R2file, "fastq")
R2_df = pd.DataFrame(R2_parsed)

DF = pd.merge(R1_df, R2_df, on = 0, how = "inner")
DF = DF.reset_index(drop = True)
N_out = DF.shape[0]
print("UMI assigned:", N_out, N_out/N_in)

path, filename = os.path.split(R1file)
handle_out = gzip.open(os.path.join(path, "UMIremoved_" + filename + ".gz"), "wt")

for ID in DF[0].values:
    record = R1_parsed[ID]
    UMI = str(R2_parsed[ID].seq)
    record.id = ID + "_" + UMI
    record.name = ID + "_" + UMI
    record.description = ID + "_" + UMI + " " + record.description.split(' ')[1]
    handle_out.write(record.format("fastq"))
        