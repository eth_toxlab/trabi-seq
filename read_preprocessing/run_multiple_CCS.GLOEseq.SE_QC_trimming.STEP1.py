'''
Dr. Vakil Takhaveev
30.01.2023

This script is for parallel implementation of the following:
1) Copying an input fastq.gz file to the analysis folder,
2) QC of the raw data,
3) Removing low-quality reads,
4) QC of the trimmed data.
.
Click-code-seq and GLOE-seq data analysis.

GLOE-seq: run the script twice, for R1 and R2. Make respective changes in the script.
'''

import pandas as pd
import argparse
import os, sys, stat
import shutil
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "The path to the folder in which the analysis is performed", required = True)
parser.add_argument("-i", "--input", help = "The path to the folder containing the input fastq.gz files", required = True)

argument = parser.parse_args()
PATH = argument.path
INPUTPATH = argument.input

bash_script_templ = '''#!/bin/bash

#SBATCH -n 1
#SBATCH --time=_TIME_
#SBATCH --job-name=QC.trimm._NAME_
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH --output=_FOLDER_/_NAME_.out
#SBATCH --error=_FOLDER_/_NAME_.err
#SBATCH --open-mode=truncate

module load fastqc/0.11.9 trimmomatic/0.38 python/3.7.4

echo '### Initial quality check'
fastqc _FILE1_ -o _FOLDER_/fastqc_out

echo '### Trimming'
# I should add CROP:101 before ILLUMINACLIP if the read length is bigger
trimmomatic SE -trimlog _FOLDER_/trim_LOG.log _FILE1_ _FILE2_ ILLUMINACLIP:/cluster/home/vtakhaveev/programs/Trimmomatic-0.39/adapters/TruSeq3-SE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:101

# the UMI of GLOE-seq stores as R2
# trimmomatic SE -trimlog _FOLDER_/trim_LOG.log _FILE1_ _FILE2_ SLIDINGWINDOW:4:15 MINLEN:10

echo '### Checking the trimming results'
fastqc _FILE2_ -o _FOLDER_/trimmed_fastqc_out
'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# Determining the maximal size among the input file
max_MB_size = 0
for SAMPLE in os.listdir(INPUTPATH):
    if SAMPLE.endswith('R1.fastq.gz'): 
        input_file = os.path.join(INPUTPATH, SAMPLE)
        MB_size = os.path.getsize(input_file)/(10**6)
        if MB_size > max_MB_size:
            max_MB_size = MB_size

for SAMPLE in os.listdir(INPUTPATH):
    if SAMPLE.endswith('R1.fastq.gz'): 
        FOLDER = os.path.join(PATH, SAMPLE)
        os.mkdir(FOLDER)
        
        os.mkdir(os.path.join(FOLDER, 'fastqc_out'))
        os.mkdir(os.path.join(FOLDER, 'trimmed_fastqc_out'))

        input_file = os.path.join(INPUTPATH, SAMPLE)
        FILE1 = os.path.join(FOLDER, SAMPLE)
        shutil.copy2(input_file, FILE1)
        os.chmod(FILE1, stat.S_IREAD) #Read by owner
        
        FILE2 = os.path.join(FOLDER, 'trimmed_' + SAMPLE)
        
        MB_size = os.path.getsize(FILE1)/(10**6)

        ### Setting the computation time
        comp_time = 3.0*MB_size/max_MB_size#1.0*
        i, d = divmod(comp_time, 1)
        i, d = int(i), int(60*d)
        if i == 0 and d < 15:
            d = 15
        comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'#HH:MM:SS for SLURM
         
        ### Setting the computation time
        comp_mem = str(int(round(8*MB_size)))
        
        if float(comp_mem) < 5000:
            comp_mem = '5000'

        print(SAMPLE, comp_time, comp_mem)

        sh_name = os.path.join(PATH, SAMPLE, "SE_CCS_data_proc_cp.sh")
        with open(sh_name, 'w') as job_script:
            job_script.write(
                replace_all(bash_script_templ, {"_NAME_" : SAMPLE, "_FOLDER_" : FOLDER, 
                                                "_FILE1_" : FILE1, "_FILE2_" : FILE2,
                                               "_TIME_" : comp_time, "_MEMORY_" : comp_mem})
            )

        os.system("sbatch < " + sh_name)
        time.sleep(2)
