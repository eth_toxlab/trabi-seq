'''
Dr. Vakil Takhaveev
17.02.2023

This script is for parallel implementation of the following:
- append_R2_to_R1_read_name_GLOEseq.py
.
GLOE-Seq data analysis.
'''

import pandas as pd
import argparse
import os
import time

###Parsing arguments
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-p", "--path", help = "The path to the folder in which the analysis is performed", required = True)
argument = parser.parse_args()

PATH = argument.path

bash_script_templ = '''#!/bin/bash

#SBATCH -n 1
#SBATCH --time=_TIME_
#SBATCH --job-name=umi_extr._NAME_
#SBATCH --mem-per-cpu=_MEMORY_
#SBATCH --output=_FOLDER_/_NAME_.umi_extr.out
#SBATCH --error=_FOLDER_/_NAME_.umi_extr.err
#SBATCH --open-mode=truncate

module load python/3.7.4

echo '### Appending the UMI stored as R2 sequence to the R1 read name'

python3.7 $HOME/DamageSeqAnalysis/ClickCodeSeq/ClickCodeSeq_scripts/ESSENTIAL_STEPS/append_R2_to_R1_read_name_GLOEseq.py -i _FILE1_ -j _FILE2_
'''

# A function to replace the names in the bash script template
def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

# Determining the maximal size among the input file
max_MB_size = 0
for SAMPLE in os.listdir(PATH):
    if "_R1" in SAMPLE:#if SAMPLE.endswith("R1_001.fastq.gz"):
        input_file = os.path.join(PATH, SAMPLE, "trimmed_" + SAMPLE.replace(".fastq.gz", ".fastq"))### Decompressed file!       
        MB_size = os.path.getsize(input_file)/(10**6)
        if MB_size > max_MB_size:
            max_MB_size = MB_size
        
for SAMPLE in os.listdir(PATH):
    if "_R1" in SAMPLE:#if SAMPLE.endswith("R1_001.fastq.gz"):
        FOLDER = os.path.join(PATH, SAMPLE)
        FILE1 = os.path.join(PATH, SAMPLE, "trimmed_" + SAMPLE.replace(".fastq.gz", ".fastq"))### Decompressed file!  
        MB_size = os.path.getsize(FILE1)/(10**6)
        
        #R2 = SAMPLE.replace("R1_001.fastq.gz", "R2_001.fastq.gz")
        R2 = SAMPLE.replace("_R1", "_R2")
        FILE2 = os.path.join(PATH, R2, "trimmed_" + R2.replace(".fastq.gz", ".fastq"))### Decompressed file!  

        ### Setting the computation time
        comp_time = 4*MB_size/max_MB_size
        i, d = divmod(comp_time, 1)
        i, d = int(i), int(60*d)
        if i == 0 and d < 30:
            d = 30
        comp_time = str(i).zfill(2) + ':' + str(d).zfill(2) + ':00'#HH:MM:SS for SLURM

        ### Setting the computation time
        comp_mem = str(int(round(3*MB_size)))

        print(SAMPLE, comp_time, comp_mem)


        sh_name = os.path.join(PATH, SAMPLE, "SE_GLOEseq_UMIextracting_cp.sh")
        with open(sh_name, 'w') as job_script:
            job_script.write(
                replace_all(bash_script_templ, {"_NAME_" : SAMPLE, "_FOLDER_" : FOLDER, "_FILE1_" : FILE1,
                                                "_FILE2_" : FILE2,
                                               "_TIME_" : comp_time, "_MEMORY_" : comp_mem})
            )

        os.system("sbatch < " + sh_name)
        time.sleep(2)
