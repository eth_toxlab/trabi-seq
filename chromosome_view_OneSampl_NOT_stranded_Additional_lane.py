'''
09.05.2023
Dr. Vakil Takhaveev

This script generates chromosomal ideograms annotated with bars in color
to visualise the distribution of the binned variable of interest throughout the genome.

Optional: an additional variable can be shown in parallel.

The resulting karyogram will not have a stranded representation.
Do not provide stranded data.

The following columns should be in the input file(s):
1) Bin
2) Bin_size
3) VARIABLE
4) Chromosome

'''

##### Importing modules #####
import os
import sys
import pandas as pd
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import seaborn as sns
import scipy
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.colors as colors
import matplotlib.cm as cm
import argparse
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.patches as patches

mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42

mpl.rcParams['font.sans-serif'] = "Arial"
mpl.rcParams['font.family'] = "sans-serif"
mpl.rcParams['mathtext.default'] = "regular"

mpl.rcParams['font.size'] = 9.5

'''
Function for making custom color maps
'''

import matplotlib.colors as mcolors

def make_colormap(seq):
    """Return a LinearSegmentedColormap
    seq: a sequence of floats and RGB-tuples. The floats should be increasing
    and in the interval (0,1).
    """
    seq = [(None,) * 3, 0.0] + list(seq) + [1.0, (None,) * 3]
    cdict = {'red': [], 'green': [], 'blue': []}
    for i, item in enumerate(seq):
        if isinstance(item, float):
            r1, g1, b1 = seq[i - 1]
            r2, g2, b2 = seq[i + 1]
            cdict['red'].append([item, r1, r2])
            cdict['green'].append([item, g1, g2])
            cdict['blue'].append([item, b1, b2])
    return mcolors.LinearSegmentedColormap('CustomMap', cdict)
 
'''
Function for runcating the color maps
'''

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = mcolors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

c = mcolors.ColorConverter().to_rgb
rvb1 = make_colormap([c('white'), c('#FFD700'), 0.5, c('#FFD700'), c('#FF0000'), 0.7, c('#FF0000'), c('black')])

##### Parsing arguments #####
parser = argparse.ArgumentParser(description = "Arguments description")
parser.add_argument("-i", "--file1", help = "File1 with counts of binned lesions", required = True)
parser.add_argument("-v", "--variable", help = "The variable of interest in File1", required = True)
parser.add_argument("-s", "--suffix", help = "The suffix to the output file name", required = True)
parser.add_argument("-o", "--outputfolder", help = "The output folder", required = True)
parser.add_argument("-c", "--chromosomesizes", help = "The file with the standard chromosome names and chromosome sizes", required = True)
parser.add_argument("-e", "--centromere", help = "The bed file with the coordinates of centromeres", required = False)
parser.add_argument("-m", "--colormap", help = "The colormap name for the variable in File1", required = False)
parser.add_argument("-l", "--lims", help = "The minimal and maxima value of the colormap for the variable in File1", required = False)
parser.add_argument("-p", "--cmapcenter", help = "The center number of lesions for the color map", required = False)

parser.add_argument("-j", "--file2", help = "File2 with counts of binned variable2", required = False)
parser.add_argument("-w", "--secondvariable", help = "The variable of interest in File2", required = False)
parser.add_argument("-n", "--secondcolormap", help = "The colormap name for the variable in File2", required = False)

parser.add_argument("-g", "--gaps", help = "Gaps in the genome assembly", required = True)

argument = parser.parse_args()

inputfile1 = argument.file1
VARIABLE = argument.variable
suffix = argument.suffix
outputfolder = argument.outputfolder
if outputfolder[-1] != "/":
    outputfolder += "/"
chromosomesizes = argument.chromosomesizes
centromerefile = argument.centromere
colormap = argument.colormap
cmaplims = argument.lims
cmapcenter = argument.cmapcenter

if cmaplims is not None:
    cmaplims = tuple(map(float, cmaplims.replace('X', '').split(',')))

inputfile2 = argument.file2
VARIABLE2 = argument.secondvariable
colormap2 = argument.secondcolormap

gaps = argument.gaps

##### Import of the binned data #####
df = pd.read_csv(inputfile1, header = 0, index_col = 0)
df2 = pd.DataFrame({})

if inputfile2 is not None:
    df2 = pd.read_csv(inputfile2, header = 0, index_col = 0)
    

##### Extracting the information about the sample and binning #####
Nbins = df.shape[0]
print(inputfile1, Nbins)


##### Importing the information about chromosome names and lengths #####
df_chr_sizes = pd.read_csv(chromosomesizes, header = None, sep = '\t')
chromosomes_to_plot = ['chr' + str(i) for i in np.arange(1, 23, 1)] + ['chrX']
df_chr_sizes = df_chr_sizes[df_chr_sizes[0].isin(chromosomes_to_plot)]

df_centromere = None
if centromerefile is not None:
    df_centromere = pd.read_csv(centromerefile, header = 0, sep = '\t')
    ####
    for row_i, row in df_centromere.iterrows():
        cen_chr = row["chrom"]
        cen_start = float(row["chromStart"])
        cen_end = float(row["chromEnd"])
        
        df.loc[(df["Chromosome"] == cen_chr) & (df["Bin"] >= cen_start) & (df["Bin"] <= cen_end), VARIABLE] = 0
    ####
    
##### Arranging the figure #####
fig = plt.figure(1, (5, 5), dpi = 500)
ax = plt.subplot(1, 1, 1)

### Organising colors
cmap = rvb1#plt.get_cmap("binary")
if colormap is not None:
    cmap = plt.get_cmap(colormap)

min_les_number = df[VARIABLE].min()
max_les_number = df[VARIABLE].max()

if cmaplims is not None:
    min_les_number = cmaplims[0]
    max_les_number = cmaplims[1]

if cmapcenter is not None:
    delta1 = float(cmapcenter) - min_les_number
    delta2 = max_les_number - float(cmapcenter)

    if delta1 >= delta2:
        max_les_number = float(cmapcenter) + delta1

    if delta1 < delta2:
        min_les_number = float(cmapcenter) - delta2

cNorm = colors.Normalize(vmin=min_les_number, vmax=max_les_number, clip=True)
scalarMap = cm.ScalarMappable(norm=cNorm, cmap=cmap)

### Organizing colors of the second variable
cmap2 = plt.get_cmap("binary")
scalarMap2 = None
if colormap2 is not None:
    cmap2 = plt.get_cmap(colormap2)
if (inputfile2 is not None) and (VARIABLE2 is not None):
    min_les_number2 = df2[VARIABLE2].min()
    max_les_number2 = df2[VARIABLE2].max()
    
    cNorm2 = colors.Normalize(vmin = min_les_number2, vmax = max_les_number2, clip=True)
    scalarMap2 = cm.ScalarMappable(norm = cNorm2, cmap = cmap2)


### Organising positions and lengths of chromosomes
x0 = 1
number_of_chromosomes = len(chromosomes_to_plot)
height = 0.5
distance_between_chromosomes = 1.4*height
c = 0

longest_chrom_length = df_chr_sizes[1].max()
ymax = number_of_chromosomes + height + distance_between_chromosomes
ymin = None

df_gaps = pd.read_csv(gaps, header = 0, sep = "\t")

### Plotting individual chromosomes
for chrom_i, chrom_name in enumerate(chromosomes_to_plot):

    y0 = number_of_chromosomes - c

    ### Chromosome boxes
    length = float(df_chr_sizes[df_chr_sizes[0] == chrom_name][1])

    ax.text(x0 - 0.01*longest_chrom_length, y0 + 0.5*height, chrom_name, ha = 'right', va = 'center', color = 'gray', alpha = 1)

    ### Plotting the binned data
    tmp = df[df['Chromosome'] == chrom_name]
    tmp = tmp.sort_values(by = "Bin")
    tmp.loc[:, "xranges"] = tmp[["Bin", "Bin_size"]].apply(tuple, axis=1)
    data_colors  = scalarMap.to_rgba(tmp[VARIABLE].tolist(), alpha=None, bytes=False, norm=True)
    data_colors = list(map(tuple, data_colors))
    ax.broken_barh(tmp["xranges"].tolist(), (y0, height), facecolor = data_colors, edgecolor = "None", linewidth = 0)

    ### Gaps
    tmp_gaps = df_gaps[(df_gaps["chrom"] == chrom_name) & (df_gaps["type"].isin(["heterochromatin", "short_arm"]))]
    #print(tmp_gaps)
    for row_i, row in tmp_gaps.iterrows():
        st = row["chromStart"]
        end = row["chromEnd"]
        rect = patches.Rectangle((st, y0), end - st, height, linewidth=0, edgecolor='None', facecolor='#D3D3D3')
        ax.add_patch(rect)
    
    
    ### Marking the position of centromeres
    if df_centromere is not None:
        st = 10**15
        end = 0

        tmp = df_centromere[df_centromere["chrom"] == chrom_name]
        for row_i, row in tmp.iterrows():
            if row["chromStart"] < st:
                st = row["chromStart"]
            if row["chromEnd"] > end:
                end = row["chromEnd"]
    
        ax.plot([st, end], [y0 + 1.15*height, y0 + 1.15*height], ls = '-', lw = 0.75, color = 'gray', alpha = 1)
    
    ### Axis ticks above Chromosome 1
    if chrom_name == "chr1":
        for i in range(0, 25*(10**7) + 1, 5*(10**7)):
            ax.plot([i, i], [y0 + 1.1*height, y0 + 1.3*height], ls = '-', lw = 1, color = 'black', alpha = 0.5)
            ax.text(i, y0 + 1.35*height, str(int(i/10**6)) + "Mb", ha = 'center', va = 'bottom', color = 'gray', alpha = 1)

    c += distance_between_chromosomes
    if chrom_i == number_of_chromosomes - 1:
        ymin = y0 - distance_between_chromosomes


ax.set_xlim(x0 - 0.025*longest_chrom_length, x0 + 1.025*longest_chrom_length)
ax.set_ylim(ymin, ymax)

plt.axis('off')

### Colorbar
cbaxes = inset_axes(ax, width="100%", height="100%", loc = 'upper left', bbox_to_anchor=(0.75, 1 - 0.8, 0.025, 0.3), bbox_transform=ax.transAxes)
cb = fig.colorbar(scalarMap, cax = cbaxes, orientation = 'vertical')
#cbaxes.tick_params(labelsize=6)
#cb.ax.tick_params(size = 0)
cb.outline.set_visible(False)
plt.setp(plt.getp(cb.ax.axes, 'yticklabels'), color = "black")
cb.set_label(r'DNA break count, ratio to the median', color = "black")

plt.tight_layout(pad=0.35)
plt.savefig(outputfolder + "Chromosome_view_" + suffix + ".pdf")
plt.close(fig)



