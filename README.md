# TRABI-seq

This code performs genome-wide analyses in the following manuscript:

**Trabectedin derails transcription-coupled nucleotide excision repair to induce DNA breaks in highly transcribed genes**
_Kook Son, Vakil Takhaveev, Visesato Mor, Hobin Yu, Emma Dillier, Nicola Zilio, Nikolai J.L. Püllen, Dmitri Ivanov, Helle D. Ulrich, Shana J. Sturla, Orlando D. Schärer_

Jupyter notebooks contain brief information in the beginning which figures of the manuscript they generate.

The scripts and notebooks herein are written by Dr. Vakil Takhaveev.
